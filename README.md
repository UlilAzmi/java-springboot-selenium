# Java Springboot Selenium

Scraping Website Tokopedia by handphone category and output result file .csv 

this project using :
- Selenium Version 3.141.59
- Java version 11
- Spring Boot 2.5.5

## Getting started

### clone this project 
```
https://gitlab.com/UlilAzmi/java-springboot-selenium.git
```

### Install Chrome Driver for selenium
This project using ChromeDriver version 94. Chrome Driver can download on
```
link download : https://sites.google.com/a/chromium.org/chromedriver/downloads
```

### Unpack chromedriver.zip

### Move chromedriver
if using mac :
```
mv chromedriver /usr/local/bin
```
if using linux: 
```
sudo mv chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver
```
### Open Directory project
```
cd java-springboot-selenium
```

### Run this project
```
mvn clean spring-boot:run
```

### Open your REST client such Postman
Request
```
[GET] localhost:8080/tokopedia
```
Response JSON
```
{
    "data": {
        "jumlah": 150,
        "message": "Scrapping berhasil!"
    }
}
```

### Result File .csv
result data exported on main dir on this project
name of file is tokopedia.csv
