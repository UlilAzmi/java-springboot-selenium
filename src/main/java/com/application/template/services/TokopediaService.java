package com.application.template.services;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;

import net.minidev.json.JSONObject;

@Service
public class TokopediaService {
	private Logger logger = LogManager.getLogger(getClass().getName());

	public JSONObject getBySelenium() {
		JSONObject res = new JSONObject();
		
		boolean ulang = true;
		int halaman = 1;
		int total = 0;
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		WebDriver driver = null;
		
		CSVWriter writer = null;;
		int countColumn = 0;
		try {
			writer = new CSVWriter(new FileWriter("tokopedia.csv"));
			String header = "Name,Price,Image,Rating,MerchantName,Location";
			String line1[] = header.split(",");
			writer.writeNext(line1);
			countColumn = line1.length + 1;
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(ulang) {
			driver = new ChromeDriver();
		    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 3);
	        
	        try {
	            driver.get("https://www.tokopedia.com/p/handphone-tablet/handphone?page=" + halaman);
	            driver.manage().window().maximize();
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            logger.info("Reloading page " + halaman);
	            for(int i = 100 ; i <= 140 ; i++) {
	            	js.executeScript("window.scrollBy(0,"+i+")");
	            	Thread.sleep(50);
	            }
	            

	            WebElement firstResult = wait.until(presenceOfElementLocated(By.className("e1nlzfl10")));
	            List<WebElement> children = firstResult.findElements(By.className("e1nlzfl3"));
	            total += children.size();
	            logger.info("Retreiving Data...");
	            for(WebElement we: children) {
	            	WebElement elementImageHeader = we.findElement(By.className("css-79elbk"));
	            	WebElement elemenLinkImage = elementImageHeader.findElement(By.cssSelector("img"));
	            	WebElement elementNama = we.findElement(By.className("css-1bjwylw"));
	            	WebElement elementHarga = we.findElement(By.className("css-o5uqvq"));
	            	List<WebElement> elementToko = we.findElements(By.className("css-1kr22w3"));
	            	List<WebElement> elementRating = we.findElements(By.className("css-177n1u3"));
	            	
	            	String []toko = new String[2];
	            	
	            	for(int a = 0 ; a < elementToko.size() ; a++) {
	            		toko[a] = elementToko.get(a).getAttribute("innerHTML");
	            	}

	            	String dataExport[] = new String[countColumn];
	        		dataExport[0] = objectToString(elementNama.getText());
	        		dataExport[1] = objectToString(elementHarga.getText());
	        		dataExport[2] = objectToString(elemenLinkImage.getAttribute("src"));
	        		dataExport[3] = objectToString(elementRating.size());
	        		dataExport[4] = objectToString(toko[1]);
	        		dataExport[5] = objectToString(toko[0]);
	        		
					writer.writeNext(dataExport);
					
	            }
	            logger.info("Retreive Done...");
	            if(halaman == 2) {
	            	ulang = false;
	            }
	            halaman++;
	        } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				driver.quit();
	        }
		}
		
		try {
			writer.flush();
			writer.close();
			logger.info("DONE...");
		} catch (IOException e) {
			e.printStackTrace();
		}
        res.put("jumlah", total);
        res.put("message", "Scrapping berhasil!");
        
        return res;
	}
	
	private String objectToString(Object object) {
		String result;
		try{
			result = object.toString();
		}catch (Exception e) {
			result = "";
		}
		if(object == null) {
			result = "";
		}
		return result;
	}
	
}






