package com.application.template.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.template.dto.Response;
import com.application.template.services.TokopediaService;

@RestController
@RequestMapping("/tokopedia")
public class TokopediaProductApi {
	
	@Autowired
	private TokopediaService tokpedService;
	
	
	@GetMapping("")
	public ResponseEntity<Response> getAll() {
		Response response = new Response();
		response.setData(tokpedService.getBySelenium());
		return ResponseEntity.ok(response);
	}
	
}








