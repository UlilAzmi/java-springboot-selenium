package com.application.template.dto;
	
import java.io.Serializable;
import org.springframework.stereotype.Component;

@Component
public class Response implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Object data = null;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}

